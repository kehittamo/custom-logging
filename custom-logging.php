<?php
/**
 * Plugin Name: Custom Logging
 *
 * @package custom-logging
 */

// Only work when needed.
if ( getenv( 'DISABLE_DEBUG_NOTICES' ) ) {

	// Hide most reports.
	error_reporting( E_ALL & ~( E_NOTICE | E_DEPRECATED | E_USER_DEPRECATED | E_USER_NOTICE | E_STRICT ) );

}

// Log custom messages.
if ( ! function_exists( 'write_log' ) ) {

	/**
	 * Write Log
	 *
	 * @return void
	 * @author Stu Miller
	 * @see http://www.stumiller.me/sending-output-to-the-wordpress-debug-log/
	 *
	 * @param mixed $log The input to log.
	 */
	if ( ! function_exists( 'write_log' ) ) {
		function write_log( $log ) {
			if ( true === WP_DEBUG ) {
				if ( is_array( $log ) || is_object( $log ) ) {
					error_log( print_r( $log, true ) );
				} else {
					error_log( $log );
				}
			}
		}
	}
}

