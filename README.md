# Custom Logging

## Installation

Install via [kpackagist](https://bitbucket.org/kehittamo/kpackagist).

## Get started

- Add ```ENABLE_DEBUG=true``` to your .env
- Use ```write_log("Hello WorldPress");```
- Tail wp-content/debug.log (with Console for example)
- Optionally also add ```DISABLE_DEBUG_NOTICES=true``` to .env for a cleaner log
